#
# ~/.bashrc
#

# If not running interactively, don't do anything
export PATH="${PATH}:${HOME}/.local/bin"
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
export PS1="\[\033[38;5;0m\][\[$(tput sgr0)\]\[\033[38;5;164m\]\w\[$(tput sgr0)\]\[\033[38;5;0m\]]\[$(tput sgr0)\] \[$(tput sgr0)\]" 
